// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },

  imports: {
    dirs: ['stores'],
  },

  modules: [
    '@pinia/nuxt',
    '@sidebase/nuxt-auth',
  ],

  pinia: {
    autoImports: [
      'defineStore',
      ['defineStore', 'definePiniaStore'],
    ],
  },

  auth: {
    origin: 'http://localhost:3000',
    enableGlobalAppMiddleware: true,
  }
});
