export default defineNuxtRouteMiddleware(async () => {
  try {
    const { status } = useAuth();
    if (status.value !== 'authenticated') return navigateTo('/login');
    const userStore = useUserStore();
    await userStore.fetchCurrent();
  } catch (error) {
    return navigateTo('/login');
  }
});
