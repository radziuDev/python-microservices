export default defineNuxtRouteMiddleware(async () => {
  try {
    const { status } = useAuth();
    if (status.value === 'authenticated') return navigateTo('/');
  } catch {}
});
