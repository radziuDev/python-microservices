interface State {
  user: {
    id: number | null,
    name: string | null,
    email: string | null,
    avatarUrl: string | null,
  },
};

export const useUserStore = defineStore({
  id: 'UserStore',
  state: (): State => {
    return {
      user: {
        id: null,
        name: null,
        email: null,
        avatarUrl: null,
      },
    };
  },

  getters: {
    getUser(state) {
      return state.user;
    },
  },

  actions: {
    async fetchCurrent() {
      const { signOut, data } = useAuth();
      const provider = useCookie('loginProvider')?.value || null;
      if (provider && data.value?.user?.email) {
        const { data: user }: any= await useFetch(
          'http://127.0.0.1:8000/api/current',
          {
            method: 'GET',
            query: {
              provider,
              email: data.value?.user?.email
            },
          },
        );

        if (user.value.id) {
          this.user.id = user.value.id;
          this.user.name = user.value.login;
          this.user.email = user.value.email;
          this.user.avatarUrl = user.value.avatarUrl;
        } else {
          useCookie('loginProvider').value = '';
          signOut();
        }
      } else {
        useCookie('loginProvider').value = '';
        signOut();
      }
    },
  },
});

