import GithubProvider from 'next-auth/providers/github'
import { NuxtAuthHandler } from "#auth";

export default NuxtAuthHandler({
  secret: 'secret',
  pages: {
    signIn: '/login',
  },
  providers: [
    // @ts-ignore
    GithubProvider.default({
      clientId: 'f89843ad2a7b7bac2178',
      clientSecret: '3fdfdb0ed5f9393eb0ab494707beb6cf1b2301b2',
    }),
  ]
})
