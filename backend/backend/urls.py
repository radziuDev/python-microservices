from django.contrib import admin
from django.urls import include, path
from django.contrib.auth.models import User, Group

admin.site.unregister(User)
admin.site.unregister(Group)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include('user.urls')),
]
