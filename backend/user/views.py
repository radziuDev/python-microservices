import requests
from rest_framework.views import APIView
from rest_framework.response import Response

from .models import User
from .serializers import UserSerializer


class getCurrentUser(APIView):
    def get(self, request, format=None):
        id = None
        user = None

        provider = request.GET.get("provider")
        if (provider == "github"):
            email = request.GET.get("email")
            url = f"https://api.github.com/search/users?q={email}"
            response = requests.get(url)
            res_json = response.json()
            id = f"github_{res_json['items'][0]['id']}"

            users = User.objects.filter(id=id)
            if users.exists():
                user = users.first()
            else:
                User.objects.create(
                    id=id,
                    email=email,
                    login=res_json["items"][0]["login"],
                    avatarUrl=res_json["items"][0]["avatar_url"]
                )
                user = User.objects.get(id=id)

            serializer = UserSerializer(user)
            return Response(serializer.data)
        else:
            return Response(None)
