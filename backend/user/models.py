from django.db import models


class User(models.Model):
    id = models.CharField(primary_key=True, max_length=255)
    email = models.EmailField()
    login = models.CharField(max_length=255)
    avatarUrl = models.CharField(max_length=255)

    class Meta:
        ordering = ('id',)
